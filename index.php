<!doctype html>
<html lang="pt">
  <head>
    <title>Hello, world!</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos1.css"/>

  </head>
  <body>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="bootstrap.min.js"></script>



    <?php
      session_start();
      if(isset($_SESSION['id']) && empty($_SESSION['id']) == false){
        require 'banco.php';
    ?>
      ADMINISTRADOR <br/>
      <hr/>
      <a href="adicionar.php" class="btn btn-success">Adicionar Novo Usuario</a><br/><br/>
      <table border="1" width="100%" class="tabela">
            <tr class="titulo_tabela">
              <th >Nome</th>
              <th>E-mail</th>
              <th>Ações</th>
            </tr>
        <?php
          $sql = "SELECT * FROM usuario";
          $sql = $pdo->query($sql);
            if ($sql -> rowCount() > 0){
              foreach($sql->fetchAll() as $usuario){
                echo '<tr>';
                  echo '<td>'.$usuario['nome'].'</td>';
                  echo '<td>'.$usuario['email'].'</td>';
                  echo '<td>
                    <a class="btn btn-info btn-xs" href="editar.php?id='.$usuario['id'].'">Editar</a>
                    <a class="btn btn-danger btn-xs" href="excluir.php?id='.$usuario['id'].'">Excluir</a>
                    </td>';
                  echo '<tr/>';
              }
            }
          } else{
            header("Location: login.php");
          }
      
        ?>
      </table>
  </body>
</html>
