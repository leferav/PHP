<script>
function refatorarAcentuacaoJS (texto) {
    texto.replaceAll('á', '\u00e1');
    texto.replaceAll('à', '\u00e0');
    texto.replaceAll('â', '\u00e2');
    texto.replaceAll('ã', '\u00e3');
    texto.replaceAll('ä', '\u00e4');
    texto.replaceAll('Á', '\u00c1');
    texto.replaceAll('À', '\u00c0');
    texto.replaceAll('Â', '\u00c2');
    texto.replaceAll('Ã', '\u00c3');
    texto.replaceAll('Ä', '\u00c4');
    texto.replaceAll('é', '\u00e9');
    texto.replaceAll('è', '\u00e8');
    texto.replaceAll('ê', '\u00ea');
    texto.replaceAll('É', '\u00c9');
    texto.replaceAll('È', '\u00c8');
    texto.replaceAll('Ê', '\u00ca');
    texto.replaceAll('Ë', '\u00cb');
    texto.replaceAll('í', '\u00ed');
    texto.replaceAll('ì', '\u00ec');
    texto.replaceAll('î', '\u00ee');
    texto.replaceAll('ï', '\u00ef');
    texto.replaceAll('Í', '\u00cd');
    texto.replaceAll('Ì', '\u00cc');
    texto.replaceAll('Î', '\u00ce');
    texto.replaceAll('Ï', '\u00cf');
    texto.replaceAll('ó', '\u00f3');
    texto.replaceAll('ò', '\u00f2');
    texto.replaceAll('ô', '\u00f4');
    texto.replaceAll('õ', '\u00f5');
    texto.replaceAll('ö', '\u00f6');
    texto.replaceAll('Ó', '\u00d3');
    texto.replaceAll('Ò', '\u00d2');
    texto.replaceAll('Ô', '\u00d4');
    texto.replaceAll('Õ', '\u00d5');
    texto.replaceAll('Ö', '\u00d6');
    texto.replaceAll('ú', '\u00fa');
    texto.replaceAll('ù', '\u00f9');
    texto.replaceAll('û', '\u00fb');
    texto.replaceAll('ü', '\u00fc');
    texto.replaceAll('Ú', '\u00da');
    texto.replaceAll('Ù', '\u00d9');
    texto.replaceAll('Û', '\u00db');
    texto.replaceAll('ç', '\u00e7');
    texto.replaceAll('Ç', '\u00c7');
    texto.replaceAll('ñ', '\u00f1');
    texto.replaceAll('Ñ', '\u00d1');
    texto.replaceAll('&', '\u0026');

    return texto;
}
</script>
