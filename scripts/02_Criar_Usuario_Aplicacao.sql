--CRIAR O USUARIO USR_PHP PARA A APLICACAO
CREATE USER 'usr_php'@'localhost' IDENTIFIED BY 'usr_php';

--DAR PERMISSOES PARA O USUARIO DA APLICACAO
GRANT ALTER, CREATE, CREATE VIEW, DELETE, DROP, EXECUTE, INDEX, INSERT,
  REFERENCES, SELECT, TRIGGER, UPDATE
ON db_php.* TO 'usr_php'@'localhost';
