DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
)

INSERT INTO `usuario` (`id`, `nome`, `email`, `senha`) VALUES
(1, 'Leandro Ferreira de Avila', 'leferav@gmail.com', '9999'),
(2, 'Miguel Angelo Tourino', 'miguel@systemhouse.com', '987'),
(3, 'Eduardo Campos', 'edcampos@systemhouse.com.br', '654'),
(4, 'Teste', 'teste@systemhouse.com.br', '321');